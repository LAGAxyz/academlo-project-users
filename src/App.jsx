import { useEffect, useState } from "react"
import axios from 'axios'
import FormUser from "./components/FormUser"
import UserCard from "./components/UserCard"
import './main.css'

function App() {
  const [users, setUsers] = useState()
  const [updateInfo, setUpdateInfo] = useState()
  const [isOpen, setIsOpen] = useState(false)

  const getAllUsers = () => {
    const url = `https://users-crud.academlo.tech/users/`;

    axios.get(url)
      .then(res => setUsers(res.data))
      .catch(error => console.log(error))
  }

  useEffect(() => {
    getAllUsers()
  }, [])

  const createNewUser = (data) => {
    const url = `https://users-crud.academlo.tech/users/`;

    axios.post(url, data)
      .then(res => {
        console.log(res.data)
        getAllUsers()
      })
      .catch(error => console.log(error))
  }

  const deleteUserById = (id) => {
    const url = `https://users-crud.academlo.tech/users/${id}/`

    axios.delete(url)
      .then(res => {
        console.log(res.data)
        getAllUsers()
      })
      .catch(error => console.log(error))
  }

  const updateUserById = (id, data) => {
    const url = `https://users-crud.academlo.tech/users/${id}/`

    axios.put(url, data)
      .then(res => {
        console.log(res.data)
        getAllUsers()
        setUpdateInfo()
      })
      .catch(error => console.log(error))
  }

  const handleOpen = ()=> setIsOpen(true)
  const handleClose = ()=> setIsOpen(false)

  return (
    <div className="App">
      <h1>ENTREGABLE 04</h1>
      <button
        className='app__btn-form'
        onClick={handleOpen}>
        Open Form
      </button>
      <div className={`app__form ${isOpen && 'app__form-visible'}`}>
        <FormUser
          createNewUser={createNewUser}
          updateInfo={updateInfo}
          updateUserById={updateUserById}
          setUpdateInfo={setUpdateInfo}
          handleClose={handleClose}
        />
      </div>
      <div className="cards">
        {
          users?.map((user) => (
            <UserCard
              key={user.id}
              user={user}
              deleteUserById={deleteUserById}
              setUpdateInfo={setUpdateInfo}
              handleOpen={handleOpen}
            />
          ))
        }
      </div>
    </div>
  )
}

export default App