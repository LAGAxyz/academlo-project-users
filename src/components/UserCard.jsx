import React from 'react'

const UserCard = ({user, deleteUserById, setUpdateInfo, handleOpen}) => {
    const handleDelete = ()=>{
        deleteUserById(user.id)
    }

    const handleUpdate = ()=>{
      setUpdateInfo(user)
      handleOpen()
    }

  return (
    <article className='card'>
        <h2 className='card__title'>{`${user.first_name} ${user.last_name}`}</h2>
        <ul className='card__list'>
            <li><span>Email: </span>{user.email}</li>
            <li><span>Birthday: </span>{user.birthday}</li>
        </ul>
        <button className='card__btn-update' onClick={handleUpdate}>Update</button>
        <button className='card__btn-delete' onClick={handleDelete}>Delete</button>
    </article>
  )
}

export default UserCard